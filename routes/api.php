<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\UserManagerController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::post('login', [UserController::class, 'login']);
Route::post('logout', [UserController::class, 'logout'])->middleware('auth:sanctum');

Route::middleware(['auth:sanctum', 'abilities:admin'])->group(function() {
	Route::get('users-manager', [UserManagerController::class, 'index']);
	Route::post('users-manager', [UserManagerController::class, 'store']);
	Route::get('users-manager/{id}', [UserManagerController::class, 'show']);
	Route::put('users-manager/{id}', [UserManagerController::class, 'update']);	
	Route::delete('users-manager/{id}', [UserManagerController::class, 'destroy']);
});

Route::middleware(['auth:sanctum', 'abilities:user'])->group(function() {
	Route::get('user', [UserController::class, 'show']);
	Route::put('user', [UserController::class, 'update']);
	Route::delete('user', [UserController::class, 'destroy']);
});
