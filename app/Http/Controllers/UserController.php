<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
	/**
	 * Display the specified resource.
	 */
	public function show(
		Request $request
	): JsonResponse
	{
		try {
			$user = $request->user();

			if (!$user) {
				return response()->json([
					'error' => 'User not Found',
				], Response::HTTP_NOT_FOUND);
			}

			return response()->json([
				'user' => $user
			], Response::HTTP_OK);
		} 
		catch (\Exception $e) {
			return response()->json([
				'error' => $e->getMessage(),
			], Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Update the specified resource in storage.
	 */
	public function update(
		Request $request
	): JsonResponse
	{
		try {
			$user = $request->user();

			if (!$user) {
				return response()->json([
					'error' => 'User not Found',
				], Response::HTTP_NOT_FOUND);
			}
	
			$user->name = $request->name ?? $user->name;
			$user->password = $request->password ? Hash::make($request->password) : $user->password;
			$user->update();

			return response()->json([
				'user' => $user
			], Response::HTTP_OK);
		}
		catch (\Exception $e) {
			return response()->json([
				'error' => $e->getMessage(),
			], Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Remove the specified resource from storage.
	 */
	public function destroy(
		Request $request
	): JsonResponse
	{
		try {
			$user = $request->user();

			if (!$user) {
				return response()->json([
					'error' => 'User not Found',
				], Response::HTTP_NOT_FOUND);
			}

			$user->tokens()->delete();
			$user->delete();

			return response()->json([
				'message' => Response::$statusTexts['200']
			], Response::HTTP_OK);
		} 
		catch (\Exception $e) {
			return response()->json([
				'error' => $e->getMessage(),
			], Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function login(
		Request $request
	): JsonResponse
	{
		try {
			$user = User::where('email', $request->email)->first();

			if (!$user || !Hash::check($request->password, $user->password)) {
				return response()->json([
					'error' => 'Invalid credentials'
				], Response::HTTP_UNAUTHORIZED);
			}
			
			$oldLoginTokens = $user->tokens()->where('personal_access_tokens.name', 'login')->get();

			foreach ($oldLoginTokens as $token) {
				$token->delete();
			}

			$token = $user->createToken('login', ['user'])->plainTextToken;

			return response()->json([
				'user' => $user,
				'token' => $token,
			], Response::HTTP_OK);
		}
		catch (\Exception $e) {
			return response()->json([
				'error' => $e->getMessage(),
			], Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function logout(
		Request $request
	): JsonResponse
	{
		try {
			$user = $request->user();
			
			$oldLoginTokens = $user->tokens()->where('personal_access_tokens.name', 'login')->get();

			foreach ($oldLoginTokens as $token) {
				$token->delete();
			}

			return response()->json([
				'message' => Response::$statusTexts['200']
			], Response::HTTP_OK);
		}
		catch (\Exception $e) {
			return response()->json([
				'error' => $e->getMessage(),
			], Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
}
