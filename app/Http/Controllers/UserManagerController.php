<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserManagerController extends Controller
{
	/**
	 * @api {get} https://procorp.crm-4.online/api/users-manager Get all users
	 * @apiVersion 1.0.0
	 * @apiGroup User manager
	 *
	 * @apiHeader {String="Bearer :token"} Authorization Replace <code>:token</code> with supplied Auth Token
	 *
	 * @apiSuccessExample Success-Response:
	 *	HTTP/1.1 200 OK
	 *	{
	 *		"users": [
	 *			{
	 *				"id": 1,
	 *				"name": "user1",
	 *				"name": "user1@example.com",
	 *			},
	 *			{
	 *				"id": 2,
	 *				"name": "user2",
	 *				"name": "user2@example.com",
	 *			}
	 *		]
	 *	}
	 */
	public function index()
	{
		try {
			$users = User::get();

			return response()->json([
				'users' => $users
			], Response::HTTP_OK);
		}
		catch (\Exception $e) {
			return response()->json([
				'error' => $e->getMessage(),
			], Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * @api {post} https://procorp.crm-4.online/api/users-manager Create user
	 * @apiVersion 1.0.0
	 * @apiGroup User manager
	 *
	 * @apiHeader {String="Bearer :token"} Authorization Replace <code>:token</code> with supplied Auth Token
	 * 
	 * @apiBody {string} name Name
	 * @apiBody {string} email Valid email
	 * @apiBody {string} password Password
	 * @apiBody {string} password_confirmation Password confirmation
	 *
	 * @apiSuccessExample Success-Response:
	 *	HTTP/1.1 201 OK
	 *	{
	 *		"user": {
	 *			"name": "user1",
	 *			"name": "user1@example.com",
	 *			"id": 1
	 *		}
	 *	}
	 */
	public function store(
		Request $request,
		Validator $validator
	): JsonResponse
	{
		$params = [
			'name' => $request->name,
			'email' => $request->email,
			'password' => $request->password,
			'password_confirmation' => $request->password_confirmation,
		];

		$rules = [
			'name' => 'required|string',
			'email' => 'required|email|string|unique:users,email',
			'password' => [
				'required',
				'string',
				'between:8,256',
				'confirmed',
				'regex:/[a-z]/', // must contain at least one lowercase letter
				'regex:/[A-Z]/', // must contain at least one uppercase letter
				'regex:/[0-9]/', // must contain at least one digit
				'regex:/[@$!%*#?&]/', // must contain a special character
			],
			'password_confirmation' => 'required|string',
		];

		$validation = $validator::make($params, $rules);

		if ($validation->fails()) {
			return response()->json([
				'error' => $validation->errors()->all()
			], Response::HTTP_BAD_REQUEST);
		}

		$user = User::create([
			'name' => $request->name,
			'email' => $request->email,
			'password' => bcrypt($request->password),
		]);

		return response()->json([
			'user' => $user
		], Response::HTTP_CREATED);
	}

	/**
	 * @api {get} https://procorp.crm-4.online/api/users-manager/:id Show user
	 * @apiVersion 1.0.0
	 * @apiGroup User manager
	 *
	 * @apiHeader {String="Bearer :token"} Authorization Replace <code>:token</code> with supplied Auth Token
	 * 
	 * @apiParam {int} id id
	 *
	 * @apiSuccessExample Success-Response:
	 *	HTTP/1.1 200 OK
	 *	{
	 *		"user": {
	 *			"name": "user1",
	 *			"name": "user1@example.com",
	 *			"id": 1
	 *		}
	 *	}
	 */
	public function show(
		int $id
	): JsonResponse
	{
		try {
			$user = User::find($id);

			if (!$user) {
				return response()->json([
					'error' => 'User not Found',
				], Response::HTTP_NOT_FOUND);
			}

			return response()->json([
				'user' => $user
			], Response::HTTP_OK);
		} 
		catch (\Exception $e) {
			return response()->json([
				'error' => $e->getMessage(),
			], Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * @api {put} https://procorp.crm-4.online/api/users-manager/:id Update user
	 * @apiVersion 1.0.0
	 * @apiGroup User manager
	 *
	 * @apiHeader {String="Bearer :token"} Authorization Replace <code>:token</code> with supplied Auth Token
	 * 
	 * @apiParam {int} id id
	 * 
	 * @apiBody {string} [name] Name
	 * @apiBody {string} [password] Password
	 *
	 * @apiSuccessExample Success-Response:
	 *	HTTP/1.1 200 OK
	 *	{
	 *		"user": {
	 *			"name": "user1",
	 *			"name": "user1@example.com",
	 *			"id": 1
	 *		}
	 *	}
	 */
	public function update(
		Request $request,
		int $id
	): JsonResponse
	{
		try {
			$user = User::find($id);

			if (!$user) {
				return response()->json([
					'error' => 'User not Found',
				], Response::HTTP_NOT_FOUND);
			}
	
			$user->name = $request->name ?? $user->name;
			$user->password = $request->password ? Hash::make($request->password) : $user->password;
			$user->update();

			return response()->json([
				'user' => $user
			], Response::HTTP_OK);
		}
		catch (\Exception $e) {
			return response()->json([
				'error' => $e->getMessage(),
			], Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * @api {delete} https://procorp.crm-4.online/api/users-manager/:id Delete user
	 * @apiVersion 1.0.0
	 * @apiGroup User manager
	 *
	 * @apiHeader {String="Bearer :token"} Authorization Replace <code>:token</code> with supplied Auth Token
	 * 
	 * @apiParam {int} id id
	 *
	 * @apiSuccessExample Success-Response:
	 *	HTTP/1.1 200 OK
	 *	{
	 *		"message": "OK"
	 *	}
	 */
	public function destroy(
		int $id
	): JsonResponse
	{
		try {
			$user = User::find($id);

			if (!$user) {
				return response()->json([
					'error' => 'User not Found',
				], Response::HTTP_NOT_FOUND);
			}

			$user->tokens()->delete();
			$user->delete();

			return response()->json([
				'message' => Response::$statusTexts['200']
			], Response::HTTP_OK);
		} 
		catch (\Exception $e) {
			return response()->json([
				'error' => $e->getMessage(),
			], Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
}
